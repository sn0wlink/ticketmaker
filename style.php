<?php
    include 'config.php';
?>

body {
    <?php echo $background ?>
}

.header {
    position: fixed;
    width: 100%;
    top:0;
    left:0;
    margin: 0 auto;
    font-family: helvetica, arial, sans;
    color: #ffffff;
    background-color: <?php echo $maincolour; ?> ;
    z-index: 1000;
}

div.form {
    font-family: helvetica, arial, sans;
    position: relative;
    padding: 30px;
    color: <?php echo $textcolour; ?>;
    border-radius: 10px;
    background-color: #ffffff;
    box-shadow: 5px 5px 10px #9aa5ad;
    margin: 0 auto;
    margin-top: 120px;
    width: 500px;
}

input.box {
    width: 100%;
    outline: 2px;
    outline-color: #000000;
    background-color: #e0e3e5;
    padding: 10px;
    border: 0;
    border-radius: 4px;
    color: <?php echo $textcolour; ?>;
    font-weight: bold;
    font-family: helvetica, arial, sans;
}

textarea {
    width: 100%;
    outline: 2px;
    outline-color: #000000;
    background-color: #e0e3e5;
    padding: 10px;
    border: 0;
    border-radius: 4px;
    color: <?php echo $textcolour; ?>;
    font-weight: bold;
    font-family: helvetica, arial, sans;
}

input.ticketsummary {
    cursor: pointer;
    width: 100%;
    padding: 0px;
    border: 0;
    background-color: #ffffff;
    color: <?php echo $textcolour; ?>;
    font-weight: bold;
    font-family: helvetica, arial, sans;
    text-align: left;
  }

input.submit {
    width: 100%;
    outline: none;
    background-color: <?php echo $maincolour; ?>;
    padding: 10px;
    border: 0;
    border-radius: 4px;
    color: <?php echo $buttontext; ?>;
    font-weight: bold;
    font-family: helvetica, arial, sans;
  }

  div.success {
    color: <?php echo $textcolour; ?>;
    font-weight: bold;
    font-family: helvetica, arial, sans;
  }

div.admin-spacer {
    height:90px;
    width: 100%;
}

div.admin-menu-container {
    margin: 0 auto;
    top: 100px;
    outline: none;
    padding: 10px;
    border: 0;
    border-radius: 4px;
    color: <?php echo $maincolour; ?>;
    font-weight: bold;
    font-family: helvetica, arial, sans;
}

a.admin-menu {
    text-decoration: none;
    color: <?php echo $maincolour; ?>;
}


a.ticketglob {
    text-decoration: none;
}

div.admin-ticket {
    font-family: helvetica, arial, sans;
    position: relative;
    color: <?php echo $textcolour; ?>;
    border-radius: 10px;
    background-color: #ffffff;
    box-shadow: 5px 5px 10px #9aa5ad;
    margin-bottom: 15px;
    width: 500px;
}

div.admin-content {
    margin: 0 auto;
    width: 500px;
}

table.ticketglob {
    width: 100%;
    padding: 10;
}

input.delete {
    cursor: pointer;
    float:right;
    font-size: 8pt;
    width: 25px;
    height: 25px;
    outline: none;
    background-color: #c11921;
    border: 0;
    border-radius: 20px;
    color: #ffffff;
    font-weight: bold;
    align-content: right;
    font-family: helvetica, arial, sans;
}

input.deleteall {
    cursor: pointer;
    font-size: 8pt;
    width: 100px;
    height: 25px;
    outline: none;
    background-color: #c11921;
    border: 0;
    border-radius: 20px;
    color: #ffffff;
    font-weight: bold;
    align-content: right;
    font-family: helvetica, arial, sans;
}

div.delete-tickets {
    margin: 0 auto;
    font-family: helvetica, arial, sans;
    position: relative;
    color: <?php echo $textcolour; ?>;
    border-radius: 10px;
    background-color: #ffffff;
    box-shadow: 5px 5px 10px #9aa5ad;
    margin-bottom: 15px;
    width: 500px;
}

div.fullscreen {
    z-index: 2001;
    background-color: rgba(0, 0, 0, 0.6);
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    padding:0;
    position: fixed;
    margin: 0 auto;
}

div.fullticket {
    top: 50px;
    z-index: 2002; 
    background-color: #ffffff;
    color: <?php echo $textcolour; ?>;
    font-family: helvetica, arial, sans;
    border-radius: 10px;
    box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.5);
    white-space: pre-line;
    position: relative;
    width:500px;
    margin: 0 auto;
    padding-top: 20px;
    padding-left: 20px;
    padding-right: 20px;
}

input.closebox {
    cursor: pointer;
    position: relative;
    z-index: 2003; 
    padding: 5px;
    margin: 0;
    float: right;
    font-size: 8pt;
    width: 25px;
    height: 25px;
    outline: none;
    background-color: #c11921;
    border: 0;
    border-radius: 20px;
    color: #ffffff;
    font-weight: bold;
    align-content: right;
    font-family: helvetica, arial, sans;
}

form.closebox {
    border:0;
}