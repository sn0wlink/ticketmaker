# Ticket Maker

The Ticket Maker is a plain text support ticket generator, with a simple interface.

## Screenshot
![image](https://gitlab.com/sn0wlink/ticketmaker/-/raw/master/ticketmaker-screenshot.png)

## News
We have just released Version 2.0. Go to the releases page to download it now!

## In the Pipeline
Any ideas welcome!

## Contribute
By all means go ahead. Just keep it clean and simple. Use 4 spaces (not tabs) and you should be good to go!

## Notes
Make sure you don't leave this on a public facing server, It WILL get abused.

This is really designed for internal company use, with a few IT staff, collaborating
on the same server.
