<?php

// This is the main site configuration page
// Have a play
// You know you want to!

// Site Name
$sitename = 'Support Ticket System';

// Colour Scheme
	// Title bar and Buttons
	$maincolour = "#3a47a2";
	// Button and title text
	$buttontext = '#ffffff';
	// Text colour
	$textcolour = "#545454";
	// Background
	$background = "background-image: linear-gradient(to right, #d4e5f4 , #b5d7f4, #79bcf7);";

// Displays Success Message
$success = "
	SUCCESS!<br /><br />

    <hr><br />

	I'll get back to,<br />
    just as soon as I figure out which one of these wires to cut?<br />
    urrr...
"
?>