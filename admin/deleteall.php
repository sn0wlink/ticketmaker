<?php
    include '../config.php';
    include 'layout.php';

    echo "<center>";
    echo "<div class='delete-tickets'>";
    echo "<br />";
    echo "<strong>Warning, This will remove everything!<br></strong>";
    echo "<br />";
    echo "<form action='deleteall.php' method='POST'>";
	echo "<input type='hidden' name='filename' value='$file'>";
    echo "<center><input class='deleteall' type='submit' name='submit' value='DELETE ALL' /></center>";
	echo "</form>";
	echo "<br />";
	echo "</div>";
	echo "</center>";

// For every ticket in folder Tickets -> then run this:
foreach (glob("../Tickets/*", GLOB_BRACE) as $file) {
	
	// Make 'Delete' Button

	if ($_POST["submit"]) {
		$path = realpath ( "$file" );
		unlink($path);
		header('Location: admin.php');
	}
}
?>