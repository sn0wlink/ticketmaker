<?php
    include '../config.php';
    include 'layout.php';

// For every ticket in folder Tickets -> then run this:
foreach (glob("../Tickets/*", GLOB_BRACE) as $file) {

	// Open each file
	$ticketcontent = file_get_contents("$file");
	$trimcontent = explode("\n", "$ticketcontent");
	$displayticket = "$trimcontent[7]\n$trimcontent[11]";

	echo "
	<div class='admin-content'>
	<form  action='admin.php' method='POST'>
	<div class='admin-ticket'>
	<table class='ticketglob'><td>
	";

	// Display ticket information
	echo "
	<input type='hidden' name='filename' value='$file'>
    <input class='ticketsummary' type='submit' name='openticket' value='$displayticket'>
	</form>
	</td><td>
	";

	echo "<form action='admin.php' method='POST'>";
	echo "<input type='hidden' name='filename' value='$file'>";
    echo "<input class='delete' type='submit' name='submit' value='X' />";
	echo "</form>";

	echo "
		</td></table>
	</div>
	</div>
	";
}

$filechosen = $_POST['filename'];

// Delete selected ticket
	if (isset($_POST["submit"])) {
		$path = realpath ( "$filechosen" );
		unlink($path);
		header("Refresh:0");
	}

// Open the selected ticket
	if (isset($_POST["openticket"])) {
		$path = realpath ( "$filechosen" );
		$printfile = file_get_contents("$filechosen");

		echo "
		<form class='closebox' action='admin.php' method='POST'>
			<div class='fullscreen'>
			<div class='fullticket'>
			<input class='closebox' type='submit' name='closebox' value='X' />
			$printfile
			</div>
			</div>
		</form>
		";
	}

	// Delete selected ticket
	if (isset($_POST["closebox"])) {
		header("Refresh:0");
	}

?>